<?php

namespace app\controllers;

use app\components\behaviors\QuestionBehavior;
use app\components\behaviors\TestBehavior;
use app\models\queries\TestQuery;
use yii;
use yii\web\Controller;
use app\models\User;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use app\models\questions\Question;
use yii\helpers\Url;
use app\models\Test;

class TestController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Start testing
     * @param string $secret - user secret
     * @return string
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionIndex($secret)  {
        $model = User::findBySecret($secret);
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        /**@var Test|TestBehavior $test*/
        $test = new Test();
        $test->user_id = $model->getPrimaryKey();

        if ($test->save()) {
            $first = $test->initQuestions();
            return $this->render('index', [
                'question' => $first
            ]);
        }

        throw new ServerErrorHttpException();
    }

    /** Next question */
    public function actionTesting() {
        $request = Yii::$app->request;
        $secret = $request->post('secret');

        if ($question = Question::findBySecret($secret)) {
            /**
             * @var Question|QuestionBehavior $type
             * @var Question|QuestionBehavior $question
             */
            $type = $question->convert();
            $type->setScenario($type->type);
            $type->load(\Yii::$app->request->post());

            if ($type->validate()) {

                if (!$type->answered) {
                    $type->setScenario($type::SCENARIO_DEFAULT);
                    $type->answered = true;
                    $type->answer = $type->getAnswer();

                    if ($type->isCorrect()) {
                        $type->points++;
                    }

                    if (!$type->save(true, ['watched', 'answered', 'points', 'answer'])) {
                        throw new ServerErrorHttpException();
                    }
                }

                if ($next = (new Test())->getNextQuestion($type)) {
                    /**@var Test|TestBehavior*/
                    return $this->render('index', [
                        'question' => $next
                    ]);
                } else {
                    $test = Test::findOne($type->test_id);
                    if (!$test->ready) {
                        $test->finish();
                    }
                    return $this->redirect(Url::to(['test/finish', 'secret' => $test->secret]));
                }

            } else {
                return $this->render('index', [
                    'question' => $type
                ]);    
            }
        }

        throw new \HttpException();
    }

    /**
     * Show results
     * @param string $secret - test secret
     * @return string
     * @throws \HttpException
     */
    public function actionFinish($secret)  {
        /**@var Test $test*/
        if (($test = Test::findBySecret($secret)) &&
            ($user = User::findOne($test->user_id))) {
            return $this->render('finish', [
                'test'  => $test,
                'user'  => $user
            ]);
        }
        throw new \HttpException();
    }
}
