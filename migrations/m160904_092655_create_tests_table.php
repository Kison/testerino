<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tests`.
 */
class m160904_092655_create_tests_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('tests', [
            'id'        => $this->primaryKey(),
            'user_id'   => $this->integer()->unsigned()->notNull(),
            'ready'     => $this->boolean()->defaultValue(false),
            'points'    => $this->integer()->defaultValue(0),
            'secret'    => $this->string(10)->notNull(),
            'started'   => $this->timestamp()->defaultValue(null),
            'finished'  => $this->timestamp()->defaultValue(null)
        ], 'ENGINE=MyISAM');
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('tests');
    }
}
