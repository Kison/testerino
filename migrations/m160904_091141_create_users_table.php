<?php

use yii\db\Migration;

/**
 * Handles the creation for table `users`.
 */
class m160904_091141_create_users_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('users', [
            'id'        => $this->primaryKey(),
            'name'      => $this->string(35)->notNull(),
            'secret'    => $this->string(10)->notNull()
        ], 'ENGINE=MyISAM');
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('users');
    }
}
