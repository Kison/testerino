<?php

use yii\db\Migration;

/**
 * Handles the creation for table `questions`.
 */
class m160904_092705_create_questions_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('questions', [
            'id'        => $this->primaryKey(),
            'test_id'   => $this->integer()->unsigned()->notNull(),
            'points'    => $this->integer()->notNull()->defaultValue(0),
            'secret'    => $this->string(10)->notNull(),
            'order'     => $this->integer(),
            'type'      => $this->string(100)->notNull(),
            'data'      => $this->text()->notNull(),
            'answered'  => $this->boolean()->defaultValue(false),
            'answer'    => $this->text(),
        ], 'ENGINE=MyISAM');
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('questions');
    }
}
