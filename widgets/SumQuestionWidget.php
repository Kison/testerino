<?php

namespace app\widgets;

use yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * SumQuestionWidget.php class file
 * @author DOL <denKison@gmail.com>
 * @date 08.09.2016
 */
class SumQuestionWidget extends QuestionWidget {
    /** @return string */
    protected function getHeaderMessage() {
        return Yii::t('app', "Calculate sum of numbers");
    }

    /**
     * @param ActiveForm $form
     * @return string
     */
    protected function getBody($form) {
        $data = $this->question->getDataArray();
        $content = Html::beginTag('div', ['class' => 'sum-question-wrapper']);
        $template = "{label}\n<div class='col-xs-2 input-wrapper center-block'>{input}</div>\n{hint}\n{error}";
        $content .= $form->field($this->question, 'sum', ['template' => $template])
            ->textInput()
            ->label("{$data['a']} + {$data['b']}", ['class' => 'text-center']);
        $content .= Html::endTag('div');
        return $content;
    }


    /** @return string */
    protected function registerCss() {
        parent::registerCss();

        $css = <<<CSS
        .sum-question-wrapper label {
            display: block;
            font-size: 20px;
        }
        
        .sum-question-wrapper .input-wrapper  {            
            float: none;
        }
        
        .sum-question-wrapper input  {            
            text-align: center;
        }
        
        .sum-question-wrapper .help-block-error {
            text-align: center;
        }
        
        .sum-question-wrapper .field-sumquestion-sum {
            margin-bottom: 0;
        }
CSS;
        $this->getView()->registerCss($css);
    }
}