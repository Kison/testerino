<?php

namespace app\widgets;

use yii;
use yii\bootstrap\ActiveForm;

/**
 * TextQuestionWidget.php class file
 * @author DOL <denKison@gmail.com>
 * @date 08.09.2016
 */
class TextQuestionWidget extends QuestionWidget {

    /** @return string */
    protected function getHeaderMessage() {
        return Yii::t('app', "Read the text");
    }

    /**
     * @param ActiveForm $form
     * @return string
     */
    protected function getBody($form) {
        return <<<TEXT
        Lorem ipsum dolor sit amet, mei agam nihil et. Te sit legere dictas suscipiantur, duo dolorum reformidans cu. 
        Nec at quis vocent phaedrum, cu sed habemus mentitum moderatius. Ad nam nemore tibique, intellegebat 
        vituperatoribus cu nec. Ne eius saepe repudiandae eos, et duis insolens vim, prima sapientem cu vel. 
        Mea in duis saepe. Mei reque vituperatoribus no.
TEXT;
    }
    
}