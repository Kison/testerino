<?php

namespace app\widgets;

use yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * CheckboxQuestionWidget.php class file
 * @author DOL <denKison@gmail.com>
 * @date 08.09.2016
 */
class CheckboxQuestionWidget extends QuestionWidget {

    /** @return string */
    protected function getHeaderMessage() {
        return Yii::t('app', "Programming languages");
    }

    /**
     * @param ActiveForm $form
     * @return string
     */
    protected function getBody($form) {
        $content = Html::beginTag('div', ['class' => 'checkbox-question-wrapper']);
        $content .= $form->field($this->question, 'languages')
            ->checkboxList($this->question->getLanguages())
            ->label("Which programming languages do you know?", ['class' => 'text-center']);
        $content .= Html::endTag('div');
        return $content;
    }


    /** @return string */
    protected function registerCss() {
        parent::registerCss();

        $css = <<<CSS
        .checkbox-question-wrapper label {
            display: block;         
        }
        
        .checkbox-question-wrapper .checkbox {                     
        }
CSS;
        $this->getView()->registerCss($css);
    }
}