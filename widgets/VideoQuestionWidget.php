<?php

namespace app\widgets;

use yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * VideoQuestionWidget.php class file
 * @author DOL <denKison@gmail.com>
 * @date 08.09.2016
 */
class VideoQuestionWidget extends QuestionWidget {

    const VIDEO_ID = '8mThJpKJJI8';

    /** @return string */
    protected function getHeaderMessage() {
        return Yii::t('app', "Watch video");
    }

    /**
     * @param ActiveForm $form
     * @return string
     */
    protected function getBody($form) {
        $content = Html::beginTag('div', ['class' => 'question-wrapper']);
        $content .= Html::beginTag('div', ['class' => 'embed-responsive embed-responsive-4by3']);
        $content .= Html::tag('div', '', ['id' => 'video-player']);
        $content .= $form->field($this->question, 'status')
            ->hiddenInput(['value' => 0, 'id' => 'video-status'])
            ->label(false);
        $content .= Html::endTag('div');
        $content .= Html::endTag('div');

        $content .= "<script>{$this->registerJs()}</script>";

        return $content;
    }

    /** @return string */
    protected function registerJs() {
        parent::registerJs();

        $id = self::VIDEO_ID;
        $js = <<<JS
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        
            var player;
            function onYouTubeIframeAPIReady() {                
                player = new YT.Player('video-player', {
                    height: '315',
                    width: '560',
                    videoId: '{$id}',
                    playerVars: {
                        controls: 0,
                        disablekb: 1
                    },
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                });
            }
        
            function onPlayerReady(event) {
                event.target.playVideo();
            }
          
            function onPlayerStateChange(event) {        
                if (event.data == YT.PlayerState.ENDED) {
                     console.log("Video finished!");
                     $('#video-status').val(1);                     
                }                                                             
            } 
JS;

        return $js;
    }
}