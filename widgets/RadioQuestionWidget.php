<?php

namespace app\widgets;

use yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * RadioQuestionWidget.php class file
 * @author DOL <denKison@gmail.com>
 * @date 08.09.2016
 */
class RadioQuestionWidget extends QuestionWidget {

    /** @return string */
    protected function getHeaderMessage() {
        return Yii::t('app', "Day of week");
    }

    /**
     * @param ActiveForm $form
     * @return string
     */
    protected function getBody($form) {
        $content = Html::beginTag('div', ['class' => 'question-wrapper']);
        $content .= $form->field($this->question, 'day')
            ->radioList($this->question->getWeekDays())
            ->label("Which day of week today?", ['class' => 'text-center']);
        $content .= Html::endTag('div');
        return $content;
    }
}