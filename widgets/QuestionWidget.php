<?php

namespace app\widgets;

use app\components\behaviors\QuestionBehavior;
use app\models\questions\Question;
use app\models\Test;
use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * QuestionWidget.php class file
 * @author DOL <denKison@gmail.com>
 * @date 05.09.2016
 */
class QuestionWidget extends Widget {

    /** @var \app\models\questions\Question|QuestionBehavior - question model */
    public $question = false;

    public function init() {
        if (!($this->question instanceof Question)) {
            throw new yii\web\ServerErrorHttpException();
        }

        $this->registerCss();
        $this->registerJs();

        parent::init();
    }

    public function run() {
        $form = ActiveForm::begin([
            'id'        => $this->question->formName(),
            'action' => $this->question->getUrl(),
            'options'   => [
                'class' => 'question-form'
            ]
        ]);
            echo Html::beginTag('div', ['class' => 'panel panel-primary question-wrapper']);
                echo Html::beginTag('div', ['class' => 'panel-heading question-header']);
                    echo $this->getHeader();
                echo Html::endTag('div');

                echo Html::beginTag('div', ['class' => 'panel-body']);
                    echo $this->getBody($form);
                    echo Html::hiddenInput('secret', $this->question->secret);
                echo Html::endTag('div');

                echo Html::beginTag('div', ['class' => 'panel-footer']);
                    echo $this->getFooter();
                echo Html::endTag('div');
            echo Html::endTag('div');
        ActiveForm::end();
    }

    /** @return string */
    protected function getHeader() {
        $points = (new Test())->getPoints($this->question->test_id);
        $content = Html::tag('span', $this->getHeaderMessage(), ['class' => 'pull-left']);
        $content .= Html::tag('span', "Your points - <b>{$points}</b>", ['class' => 'pull-right']);
        $content .= Html::tag('div', "", ['class' => 'clearfix']);
        return $content;
    }

    protected function getHeaderMessage() {
        return "Header message";
    }

    /**
     * @param ActiveForm $form
     * @return string
     */
    protected function getBody($form) {
        return "Form fields";
    }

    /** @return string */
    protected function getFooter() {
        $content = Html::submitButton('Next', ['class' => 'btn btn-primary pull-right']);
        $content .= Html::tag('div', "", ['class' => 'clearfix']);
        return $content;
    }

    /** @return string */
    protected function registerCss() {
        $css = <<<CSS
        
        .question-wrapper label {
            display: block;         
        }
        
        .question-wrapper {
            margin-bottom: 0;
        }
CSS;
        $this->getView()->registerCss($css);
    }

    /** @return string */
    protected function registerJs() {
        $js = <<<JS
        /*$("from#{$this->question->formName()}").on('beforeSubmit', function() {                      
            return false;
        });*/
JS;
        $this->getView()->registerJs($js);
    }
}