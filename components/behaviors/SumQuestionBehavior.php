<?php

namespace app\components\behaviors;


use yii;
use yii\helpers\Url;
use app\models\questions\Question;
use app\models\questions\SumQuestion;

/** Sum question behavior */
class SumQuestionBehavior extends QuestionBehavior 
    implements IQuestionBehavior {

    /** @inheritdoc */
    public function getQuestionData($encode = false) {
        /**@var Question $owner */
        $owner = $this->owner;
        $data = (!$owner->getAttribute('data')) ?
            $this->prepareQuestionData() : unserialize($owner->getAttribute('data'));
        return ($encode) ?
            serialize($data) : $data;
    }

    /** @inheritdoc */
    public function prepareQuestionData() {
        $a = rand(1, 100);
        $b = rand(1, 100);
        $data = [
            'a' => $a,
            'b' => $b,
            'c' => $a + $b
        ];
        return $data;
    }
    
    /** @inheritdoc */
    public function isCorrect() {
        $data = $this->getQuestionData();
        /**@var SumQuestion $owner */
        $owner = $this->owner;
        return ($owner->sum == $data['c']);
    }

    public function getAnswer() {
        /**@var SumQuestion $owner */
        $owner = $this->owner;
        return serialize(['answer' => $owner->sum]);
    }
}
