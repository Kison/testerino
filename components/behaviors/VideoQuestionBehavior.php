<?php

namespace app\components\behaviors;


use yii;
use app\models\questions\VideoQuestion;

/** Video question behavior */
class VideoQuestionBehavior extends QuestionBehavior
    implements IQuestionBehavior {

    /** @inheritdoc */
    public function isCorrect() {
        /**@var VideoQuestion $owner */
        $owner = $this->owner;
        return (int)$owner->status;
    }

    public function getAnswer() {
        /**@var VideoQuestion $owner */
        $owner = $this->owner;
        return serialize(['watched' => (int)$owner->status]);
    }

    public function getVideoStatusUrl() {
        return yii\helpers\Url::to(['video/status']);
    }
}
