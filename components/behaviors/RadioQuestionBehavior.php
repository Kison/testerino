<?php

namespace app\components\behaviors;

use yii;
use app\models\questions\RadioQuestion;

/** Week day question behavior */
class RadioQuestionBehavior extends QuestionBehavior
    implements IQuestionBehavior {

    /** @inheritdoc */
    public function isCorrect() {
        $current = date("w", time());
        /**@var RadioQuestion $owner */
        $owner = $this->owner;
        return ((int)$owner->day == (int)$current);
    }

    public function getAnswer() {
        /**@var RadioQuestion $owner */
        $owner = $this->owner;
        return serialize(['answer' => $owner->day]);
    }
}
