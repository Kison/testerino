<?php

namespace app\components\behaviors;

use yii;
use app\models\questions\Question;

/** Question behaviour interface */
interface IQuestionBehavior {

    /**
     * Question data getter
     * @param bool $encode
     * @return mixed
     */
    public function getQuestionData($encode = false);

    /**
     * Prepare initial data
     * @return mixed
     */
    public function prepareQuestionData();

    /**
     * Check if answer correct
     * @return boolean
     */
    public function isCorrect();

    /**
     * Get user answer
     * @return boolean
     */
    public function getAnswer();

    /**
     * Get url to action that will process answer
     * @return string
     */
    public function getUrl();
}
