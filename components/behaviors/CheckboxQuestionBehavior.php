<?php

namespace app\components\behaviors;

use yii;
use app\models\questions\CheckboxQuestion;

/**  Checkbox question behavior */
class CheckboxQuestionBehavior extends QuestionBehavior
    implements IQuestionBehavior {

    /** @inheritdoc */
    public function isCorrect() {
        /**@var CheckboxQuestion $owner */
        $owner = $this->owner;
        return (
            is_array($owner->languages) &&
            count($owner->languages) &&
            !in_array('basic', $owner->languages)
        );
    }

    public function getAnswer() {
        /**@var CheckboxQuestion $owner */
        $owner = $this->owner;
        return serialize(['answer' => $owner->languages]);
    }
}
