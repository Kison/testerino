<?php

namespace app\components\behaviors;

use yii;
use app\models\questions\QuestionsFactory;
use app\models\questions\Question;

/** Base question behavior */
class QuestionBehavior extends yii\base\Behavior
    implements IQuestionBehavior {

    
    /** @inheritdoc */
    public function getQuestionData($encode = false) {
        return ($encode) ?
            serialize([]) : [];
    }

    /**
     * Get question data
     * @return string
     */
    public function getDataArray() {
        /**@var Question $owner*/
        $owner = $this->owner;
        return (is_array($owner->data)) ?
            $owner->data :
            unserialize($owner->data);
    }

    /** @inheritdoc */
    public function prepareQuestionData() {
        return [];
    }

    /** @inheritdoc */
    public function isCorrect() {
        return true;
    }

    public function getUrl() {
        return yii\helpers\Url::to(['test/testing']);
    }
    
    public function getAnswer() {
        return serialize([]);
    }
    
    /**
     * Convert question to another type
     * @return Question
     * @throws \Exception
     */
    public function convert() {
        /**@var Question $owner*/
        $owner = $this->owner;
        $type = QuestionsFactory::getInstance($owner->type);
        $type->setAttributes($owner->getAttributes(), false);
        $type->setOldAttributes($owner->getOldAttributes());
        $type->afterFind();
        return $type;
    }
}
