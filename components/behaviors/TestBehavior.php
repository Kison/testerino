<?php

namespace app\components\behaviors;

use yii;
use app\models;
use app\models\questions\Question;
use app\models\questions\QuestionsFactory;
use yii\web\ServerErrorHttpException;

/** Test behavior */
class TestBehavior extends yii\base\Behavior {

    /**
     * Initiate test questions
     * @return Question
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    public function initQuestions() {
        /**@var models\Test $owner */
        $owner = $this->owner;
        $questions = [];
        $types = [
            Question::TYPE_TEXT,
            Question::TYPE_SUM,
            Question::TYPE_CHECKBOX,
            Question::TYPE_RADIO,
            Question::TYPE_VIDEO
        ];

        $transaction = $owner::getDb()->beginTransaction();
        try {
            shuffle($types);
            $order = 0;
            foreach ($types as $type) {
                /**@var Question|QuestionBehavior $model */
                $model = QuestionsFactory::getInstance($type);
                $model->test_id = $owner->id;
                $model->type = $type;
                $model->order = $order;
                $model->data = $model->getQuestionData(true);
                $order++;

                if ($model->save()) {
                    $questions[] = $model;
                } else {
                    throw new ServerErrorHttpException();
                }
            }

            $transaction->commit();
        } catch(\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return reset($questions);
    }

    /**
     * Get next question
     * @param Question $current - current question
     * @return boolean|Question - next question or false if test complete
     */
    public function getNextQuestion(Question $current) {
        $where = [
            'test_id'   => $current->test_id,
            'answered'  => 0
        ];
        
        /**@var Question|QuestionBehavior $model*/
        $model = Question::find()
            ->where($where)
            ->orderBy(['order' => SORT_ASC])
            ->one();

        return ($model) ?
            $model->convert() : false;
    }
}
