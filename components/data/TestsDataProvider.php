<?php

namespace app\components\data;

use yii;
use app\models\Test;

class TestsDataProvider extends yii\data\ArrayDataProvider {

    public function init() {
        $query = Test::find()
            ->where(['ready' => 1])
            ->limit(10)
            ->orderBy(['finished' => SORT_DESC]);
        
        foreach($query->all() as $test) {
            $this->allModels[] = [
                'name'      => $test->getUser()->one()->name,
                'points'    => $test->points,
                'time'      => $test->getTestTimeInSeconds() . " sec",
            ];
        }
    }
}