<?php

namespace app\models\questions;


use yii;
use app\components\behaviors\VideoQuestionBehavior;

/** This is the model class for CheckboxQuestion */
class VideoQuestion extends Question {

    const SCENARIO_VIDEO = 'video';

    public $status = 0;

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_VIDEO] = ['status'];
        return $scenarios;
    }

    public function rules() {
        return yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['day'], 'in', 'range' => [0, 1], 'on' => self::SCENARIO_VIDEO],
        ]);
    }

    public function behaviors() {
        return [
            'myLogic' => VideoQuestionBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [];
    }
}
