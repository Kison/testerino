<?php

namespace app\models\questions;

use app\models\questions\Question;

/**
 * QuestionsFactory.php class file
 * @author DOL <denKison@gmail.com>
 * @date 05.09.2016
 */
class QuestionsFactory {

    /**
     * Get appropriate question instance
     * @param string $type - question type
     * @return Question
     * @throws \Exception
     */
    public static function getInstance($type) {
        $type = ucfirst($type);
        $className = "app\\models\\questions\\{$type}Question";
        if (class_exists($className)) {
            return new $className();
        }
        throw new \Exception("{$className} question class not exists");
    }

    /**
     * Get appropriate for question widget
     * @param string $type - question type
     * @param array $config - widget config data
     * @return string
     * @throws \Exception
     */
    public static function getWidgetInstance($type, array $config = []) {
        $type = ucfirst($type);
        $className = "app\\widgets\\{$type}QuestionWidget";
        if (class_exists($className)) {
            return call_user_func([$className, 'widget'], $config);
        }
        throw new \Exception("{$className} widget class not exists");
    }
}