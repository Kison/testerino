<?php

namespace app\models\questions;

use yii;
use app\components\behaviors\TextQuestionBehavior;

/**
 * This is the model class for TextQuestion
 */
class TextQuestion extends Question {

    const SCENARIO_TEXT = 'text';
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_TEXT] = [];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return yii\helpers\ArrayHelper::merge(parent::rules(), []);
    }

    public function behaviors() {
        return [
            'myLogic' => TextQuestionBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [];
    }
}
