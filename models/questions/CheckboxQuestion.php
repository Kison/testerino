<?php

namespace app\models\questions;

use yii;
use app\components\behaviors\CheckboxQuestionBehavior;

/** This is the model class for CheckboxQuestion */
class CheckboxQuestion extends Question {

    const SCENARIO_CHECKBOX = 'checkbox';

    public $languages;

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CHECKBOX] = ['languages'];
        return $scenarios;
    }

    public function rules() {
        return yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['languages'], 'safe', 'on' => self::SCENARIO_CHECKBOX],
            [['languages'], 'filterLanguages', 'on' => self::SCENARIO_CHECKBOX],
        ]);
    }

    public function behaviors() {
        return [
            'myLogic' => CheckboxQuestionBehavior::className(),
        ];
    }
    
    public function getLanguages() {
        return [
            "php"       => "PHP",
            "python"    => "Python",
            "js"        => "JS",
            ".net"      => ".net",
            "basic"     => "Visual Basic"
        ];
    }

    /**
     * Filter languages
     * @param $attribute
     * @param $params
     */
    public function filterLanguages($attribute, $params) {
        $filtered = [];
        $langs = $this->getLanguages();
        foreach ($this->languages as $language) {
            if (array_key_exists($language, $langs)) {
                $filtered[] = $language;
            }
        }
        $this->languages = $filtered;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [];
    }
}
