<?php

namespace app\models\questions;

use yii;
use app\components\behaviors\SumQuestionBehavior;

/** This is the model class for SumQuestion */
class SumQuestion extends Question {

    const SCENARIO_SUM = 'sum';

    public $sum;

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_SUM] = ['sum'];
        return $scenarios;
    }

    public function rules() {
        return yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['sum'], 'required', 'on' => self::SCENARIO_SUM],
            [['sum'], 'integer', 'on' => self::SCENARIO_SUM],
        ]);
    }

    public function behaviors() {
        return [
            'myLogic' => SumQuestionBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [];
    }
}
