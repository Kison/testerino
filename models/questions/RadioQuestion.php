<?php

namespace app\models\questions;

use yii;
use app\components\behaviors\RadioQuestionBehavior;

/** This is the model class for CheckboxQuestion */
class RadioQuestion extends Question {

    const SCENARIO_RADIO = 'radio';

    public $day;

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_RADIO] = ['day'];
        return $scenarios;
    }

    public function rules() {
        return yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['day'], 'in', 'range' => range(1, 7), 'on' => self::SCENARIO_RADIO],
        ]);
    }

    public function behaviors() {
        return [
            'myLogic' => RadioQuestionBehavior::className(),
        ];
    }
    
    public function getWeekDays() {
        $weekDays = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday'
        ];

        $currentWeekDayIndex = (int)date("w", time());
        while (count($weekDays) > 4) {
            $keys = array_keys($weekDays);
            $index = rand(0, count($keys) - 1);
            if ($keys[$index] != $currentWeekDayIndex) {
                unset($weekDays[$keys[$index]]);
            }
        }

        return $this->shuffleKeys($weekDays);
    }

    function shuffleKeys($days) {
        $keys = array_keys($days);
        shuffle($keys);
        $shuffled = [];
        foreach ($keys as $key) {
            $shuffled[$key] = $days[$key];
        }
        return $shuffled;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [];
    }
}
