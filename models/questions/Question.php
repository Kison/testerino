<?php

namespace app\models\questions;

use yii;
use app\models\Base;
use app\models\queries\QuestionQuery;
use app\components\behaviors\QuestionBehavior;

/**
 * This is the model class for table "questions".
 *
 * @property integer $id
 * @property string $test_id
 * @property integer $points
 * @property string $type
 * @property integer $order
 * @property string $data
 * @property boolean $watched
 * @property boolean $answered
 * @property boolean $answer
 */
class Question extends Base {

    const TYPE_TEXT         = 'text';
    const TYPE_SUM          = 'sum';
    const TYPE_CHECKBOX     = 'checkbox';
    const TYPE_RADIO        = 'radio';
    const TYPE_VIDEO        = 'video';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'questions';
    }

    public function behaviors() {
        return [
            'myLogic' => QuestionBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['test_id', 'type', 'order', 'data'], 'required'],
            [['test_id', 'points', 'order'], 'integer'],
            [['type'], 'string', 'max' => 100],
            [['data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'points'    => Yii::t('app', 'Points'),
            'type'      => Yii::t('app', 'Type'),
        ];
    }
    
    /**
     * @inheritdoc
     * @return QuestionQuery the active query used by this AR class.
     */
    public static function find() {
        return new QuestionQuery(get_called_class());
    }
}
