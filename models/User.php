<?php

namespace app\models;

use yii;
use app\models\queries\UserQuery;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 */
class User extends Base {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required', 'message' => Yii::t('app', 'Name cannot be blank')],
            [['name'], 'string', 'max' => 35, 'message' => Yii::t('app', 'Name ')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => Yii::t('app', 'Please enter your name to start testing'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find() {
        return new UserQuery(get_called_class());
    }
}
