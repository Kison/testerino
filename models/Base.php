<?php

namespace app\models;

use yii;
use app\helpers\SecureHelper;

/**
 * This is the base model class
 * @property string $secret
 */
class Base extends yii\db\ActiveRecord {

    public function init(){
        parent::init();
        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'addSecret']);
    }

    /** Generate test secret, need for hide test id */
    protected function addSecret() {
        $this->secret = SecureHelper::getSecret();
    }

    /**
     * @inheritdoc
     * @return static|null ActiveRecord instance matching the condition, or `null` if nothing matches.
     */
    public static function findBySecret($secret) {
        return self::find()
            ->where(['secret' => $secret])
            ->one();
    }
}
