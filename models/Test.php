<?php

namespace app\models;

use yii;
use app\models\questions\Question;
use app\models\User;
use app\models\queries\TestQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\components\behaviors\TestBehavior;


/**
 * This is the model class for table "tests".
 *
 * @method Question getNextQuestion(Question $current)
 * 
 * @property integer $id
 * @property string $user_id
 * @property string $secret
 * @property integer $ready
 * @property integer points
 * @property string $started
 * @property string $finished
 */
class Test extends Base {

    public function behaviors() {
        return [
            'myLogic' => TestBehavior::className(),
        ];
    }

    public function init(){
        parent::init();
        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'addSecret']);
        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'addStarted']);
    }

    public function addStarted() {
        $this->started = new Expression('NOW()');
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'tests';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['started', 'finished'], 'safe'],
        ];
    }

    public function finish() {
        $this->setScenario(self::SCENARIO_DEFAULT);
        $this->ready = true;
        $this->finished = new Expression('NOW()');
        $this->points = $this->getPoints($this->id);
        return $this->save(true, ['ready', 'points', 'finished']);
    }

    /**
     * Get related user
     * @return User
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Get points count by test id
     * @param integer $id - test id
     * @return integer
     */
    public function getPoints($id) {
         $sum = Question::find()
            ->where(['test_id' => $id])
            ->sum('points');

        return (is_numeric($sum)) ? $sum : 0;
    }

    /**
     * Get test time in seconds
     * @return integer
     */
    public function getTestTimeInSeconds() {
        if ($this->ready) {
            return strtotime($this->finished) -
                strtotime($this->started);
        }
        return 0;
    }

    /**
     * @inheritdoc
     * @return TestQuery the active query used by this AR class.
     */
    public static function find() {
        return new TestQuery(get_called_class());
    }
}
