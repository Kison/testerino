<?php

namespace app\helpers;

/**
 * Secure class
 * @author DOL <denis.oleinik@cagoi.com>
 */
class SecureHelper {

    // Salt length
    const SECRET_LENGTH = 10;

    /**
     * Prepare secret
     * @return string - secret
     */
    public static function getSecret() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < self::SECRET_LENGTH; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}