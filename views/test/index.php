<?php
    use \app\models\questions\QuestionsFactory;

    /**
     * @var $this yii\web\View     
     * @var \app\models\questions\Question|\app\components\behaviors\QuestionBehavior $question
     * @var \app\models\Test $test
     * @var \app\models\User $user
     */
    $this->title = 'Testing...';


    echo QuestionsFactory::getWidgetInstance($question->type, [
        'question' => $question
    ]);






