<?php
    use yii\helpers\Html;
    use app\models\Test;
    use app\models\User;

    /**
     * @var $this yii\web\View
     * @var $user app\models\User
     * @var $test app\models\Test
     */
    $this->title = 'Testing finished!';
?>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Testing finished!')?></h3>
    </div>
    <div class="panel-body text-center">
        <p>Congratulations, <b><?= $user->name?></b></p>
        <p>You earned <b><?= $test->points ?></b> point(s)</p>
        <p>The total time of the test <b><?= $test->getTestTimeInSeconds() ?></b> second(s)</p>
        <?= Html::a(Yii::t('app', 'Go to the start'), \yii\helpers\Url::to(['index/start'])) ?>
    </div>
</div>







