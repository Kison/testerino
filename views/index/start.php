<?php
    /**
     * @var $this yii\web\View
     * @var $model app\models\User
     */
    $this->title = 'Testing tool';

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $form = ActiveForm::begin([
        'id'        => 'name-form',
        'options'   => ['class' => 'name-form'],
    ]);
?>

<?= $form->field($model, 'name') ?>

<div class="text-center">
    <?= Html::submitButton('Next', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end() ?>

<div class="row">
    <div class="col-md-12">
        <h2>Previous 10 results</h2>
        <div class="grid-wrapper">
            <?php
                echo yii\grid\GridView::widget([
                    'summary'       => false,
                    'dataProvider'  => new app\components\data\TestsDataProvider()
                ]);
            ?>
        </div>
    </div>
</div>



